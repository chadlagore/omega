--- Recreates the matchup_aggregate table daily.

CREATE TABLE matchup_aggregate AS (
    --- Collect teams and boolean for home win.
    WITH home_wins as (
        SELECT home as team,
            visitor as opponent,
            home_pts > visitor_pts AS home_win,
            home_pts
        FROM matchup
    ),

    --- Collect teams and boolean for visiting win.
    visitor_wins as (
        SELECT visitor as team,
            home as opponent,
            visitor_pts > home_pts AS visitor_win,
            visitor_pts
        FROM matchup
    ),

    --- Filter home wins.
    home_win_counts as (
        SELECT team, opponent, count(*) as home_wins
        FROM home_wins
        WHERE home_win = true
        GROUP BY team, opponent
    ),

    --- Filter visiting wins.
    visitor_win_counts as (
        SELECT team, opponent, count(*) as visitor_wins
        FROM visitor_wins
        WHERE visitor_win = true
        GROUP BY team, opponent
    ),

    --- Join win counts.
    result as (
        SELECT * FROM home_win_counts
        FULL JOIN visitor_win_counts USING (team, opponent)
        order by home_win_counts.team, visitor_win_counts.opponent
    )

    --- Backfill nulls with zeros.
    SELECT team, opponent,
           coalesce(GREATEST(home_wins, 0)) as home_wins,
           coalesce(GREATEST(visitor_wins, 0)) as visiting_wins
    FROM result
);
