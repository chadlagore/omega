-- Deletes dupes among subset of columns.

DELETE FROM matchup
WHERE id IN (SELECT id
              FROM (SELECT id,
                             ROW_NUMBER() OVER (
                                 partition BY datestring, timestring,
                                 visitor, home ORDER BY id) AS rnum
                     FROM matchup) t
              WHERE t.rnum > 1);
