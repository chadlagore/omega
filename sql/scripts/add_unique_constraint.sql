-- Adds a unique constraint to several columns.

ALTER TABLE basic_box_raw
ADD CONSTRAINT unique_datestring_teams_player
UNIQUE (datestring, home, visitor, player);
