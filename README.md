# Omega

Omega is a spider for crawling http://www.basketball-reference.com and
collecting NBA player and matchup statistics. You will need to provision
a database to run Omega. It is tested using PostgreSQL.

You simply need to set the `DATABASE_URL` environment variable to configure
the database.

### Setting Up The Environment

Omega uses [Miniconda](https://conda.io/miniconda.html) for dependency management.
Assuming you have installed Miniconda, run the following:

```bash
conda env create -f environment.yml
source activate omega
```

### Running the Spiders

If your environment is activated (see above), run:

```
scrapy crawl player  # Run the player spider.
scrapy crawl matchup # Run the matchup spider.
```
