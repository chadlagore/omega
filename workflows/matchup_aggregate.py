'''
(Re)builds the matchup_aggregate table daily.

'''

import os, sys

import luigi
from sqlalchemy.orm import sessionmaker, scoped_session
import sqlalchemy

# Run from command line, add base dir to path to do relative imports.
sys.path.append(os.path.normpath(
    os.path.dirname(os.path.join(os.getcwd(), '..'))
))

# Import directories.
from omega.settings import BASE_DIR, WORKFLOW_DIR, SQL_DIR

# Import models and strings from local modules.
from omega.models import MatchupAggregate
from strings import *

# Database engine.
url = os.environ['DATABASE_URL']
engine = sqlalchemy.create_engine(url)


class DropMatchupAggregateTask(luigi.Task):

    def run(self):
        '''
        Drops the SQL table.
        '''

        try:
            MatchupAggregate.__table__.drop(engine)
        except sqlalchemy.exc.ProgrammingError:
            pass

    def output(self):
        return luigi.LocalTarget('luigi.txt')

    def complete(self):
        return not engine.dialect.has_table(engine, MATCHUP_AGGREGATE)


class MatchupAggregateTask(luigi.Task):

    def requires(self):
        '''
        Dependent on table drop.
        '''
        return DropMatchupAggregateTask()

    def run(self):
        '''
        Opens and executes the SQL query.

        '''

        # Location of SQL query.
        matchup_aggregate_sql = os.path.join(
            SQL_DIR, 'matchup_aggregate.sql'
        )

        with open(matchup_aggregate_sql) as infile:
            query = infile.read()

        # Generate a session for writes.
        Session = scoped_session(sessionmaker(bind=engine))
        session = Session()

        try:
            session.execute(query)
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()

    def output(self):
        return luigi.LocalTarget('luigi.txt')


if __name__ == '__main__':
    luigi.run(["--local-scheduler"], main_task_cls=MatchupAggregateTask)
