import datetime

from scrapy import Spider, Request

from omega.settings import BOX_DAILY_MODE, BOX_MONTH, LOOK_BACK_DAYS, MONTHS
from omega.items import MatchupItem
from strings import *


class MatchupSpider(Spider):
    '''
    Spider for collecting matchup tables.

    '''

    name = MATCHUP
    base_url = 'http://www.basketball-reference.com/'

    # Select the months url, depending on our mode.
    month_ix = datetime.datetime.now().month if BOX_DAILY_MODE else BOX_MONTH
    month_name = MONTHS[month_ix-1] if BOX_DAILY_MODE else BOX_MONTH
    path = f'leagues/NBA_2018_games-{month_name}.html'

    # Set up xpaths for row data.
    rows_paths = {
        'datestring': 'th[@data-stat="date_game"]/a/text()',
        'timestring': 'td[@data-stat="game_start_time"]/text()',
        'visitor': 'td[@data-stat="visitor_team_name"]/a/text()',
        'visitor_pts': 'td[@data-stat="visitor_pts"]/text()',
        'home': 'td[@data-stat="home_team_name"]/a/text()',
        'home_pts': 'td[@data-stat="home_pts"]/text()',
        'box_url': 'td[@data-stat="box_score_text"]/a/@href',
        'ot': 'td[@data-stat="overtimes"]/text()'
    }

    def start_requests(self):
        '''
        Yields a request for the table page.

        '''

        yield Request(
            url=self.base_url + self.path,
            callback=self.parse
        )

    def parse(self, response):
        '''
        Parses the table page.

        '''

        # Collect the main table.
        tbl_name = 'suppress_glossary sortable stats_table'
        table = response.xpath(f'//table[@class="{tbl_name}"]')

        # Row by row, yield records.
        for row in table.xpath('tbody/tr'):
            item = MatchupItem()

            # Use the paths specified in `row_paths`.
            for column, path in self.rows_paths.items():
                item[column] = row.xpath(path).extract_first()

            # Parse the datetime.
            try:
                item['datetime'] = self.parse_date(
                    item['datestring'],
                    item['timestring']
                )
            except TypeError as e:
                print(f"Could not parse date: {item['datestring']}")
                print(f"Or could not parse date: {item['timestring']}")
                print("ITEM FAILURE: ")
                print(item)
                continue

            # Parse the OT count.
            item['ot_count'] = self.parse_ot(item['ot'])
            item['url'] = self.path
            item['month'] = self.month_name

            # Yield the record.
            yield item

    def parse_date(self, datestring, timestring):
        '''
        Parses datetime from a datestring and a timestring.

        '''

        # Collect dates, add them, return result.
        start = datetime.datetime.strptime(timestring, '%I:%M %p')
        date = datetime.datetime.strptime(datestring, '%a, %b %d, %Y')

        return date.replace(hour=start.hour, minute=start.minute)

    def parse_ot(self, ot):
        '''
        Returns the number of overtime periods.

        '''

        # May be None.
        if not ot:
            return 0
        elif ot == 'OT':
            return 1
        else:
            return int(ot.strip('OT'))
