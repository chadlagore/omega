from __future__ import absolute_import

from datetime import datetime, timedelta
import json

import scrapy
from requests.compat import urljoin, urlencode

from ..items import PlayerBasicItem, PlayerAdvancedItem
from omega.settings import BOX_DAILY_MODE, BOX_MONTH, LOOK_BACK_DAYS
from strings import *

class PlayerBasicSpider(scrapy.Spider):
    '''
    Collects box scores from http://www.basketball-reference.com/

    '''
    name = PLAYER
    base_url = 'http://www.basketball-reference.com'
    box_path = 'boxscores'
    basic_headers = [
        'mp',
        'fg',
        'fg_pct',
        'fga',
        'fg3',
        'fg3a',
        'ft_pct',
        'orb',
        'drb',
        'trb',
        'ast',
        'stl',
        'blk',
        'tov',
        'pf',
        'pts'
    ]

    advanced_headers = [
        'mp',
        'ts_pct',
        'efg_pct',
        'fg3a_per_fga_pct',
        'fta_per_fga_pct',
        'orb_pct',
        'drb_pct',
        'trb_pct',
        'ast_pct',
        'stl_pct',
        'blk_pct',
        'tov_pct',
        'usg_pct',
        'off_rtg',
        'def_rtg'
    ]

    months = [
        'january',
        'february',
        'march',
        'april',
        'may',
        'june',
        'july',
        'august',
        'september',
        'october',
        'november',
        'december'
    ]

    day = datetime.now() - timedelta(days=LOOK_BACK_DAYS)

    def start_requests(self):
        '''
        Submits one request for yesterdays boxscores.
        This request spawns one child process for each game played yeterday.

        '''

        # Figure out what month we're querying.
        month_index = datetime.now().month - 1
        month = self.months[month_index] if BOX_DAILY_MODE else BOX_MONTH
        monthly_urls = [
            self.base_url + f'/leagues/NBA_2017_games-{month}.html'
        ]

        # Maybe we need to add on last month.
        if self.day.month != (month_index + 1) and BOX_DAILY_MODE:
            month = self.months[self.day.month - 1]
            monthly_urls.append(
                self.base_url + f'/leagues/NBA_2017_games-{month}.html'
            )

        for url in monthly_urls:
            # Yield a single request with the boxscore url.
            yield scrapy.Request(
                url=url,
                callback=self.create_children
            )

    def create_children(self, response):
        '''
        Creates child processes to collect each game.

        '''

        # Extracts monthly game table.
        table_name = 'suppress_glossary sortable stats_table'
        table = response.xpath(f'//table[@class="{table_name}"]')

        # Set up xpaths for row data.
        date_path = 'th[@data-stat="date_game"]/a/text()'
        start_time_path = 'td[@data-stat="game_start_time"]/text()'
        visitor_path = 'td[@data-stat="visitor_team_name"]/a/text()'
        home_path = 'td[@data-stat="home_team_name"]/a/text()'
        link_path = 'td[@data-stat="box_score_text"]/a/@href'

        # Look at each row. Perhaps turn it into a record.
        for row in table.xpath('tbody/tr'):
            meta = {
                'date': row.xpath(date_path).extract_first(),
                'start': row.xpath(start_time_path).extract_first(),
                'visitor': row.xpath(visitor_path).extract_first(),
                'home': row.xpath(home_path).extract_first(),
                'url': row.xpath(link_path).extract_first()
            }

            # Skip header row.
            if not meta['url']:
                continue

            start = datetime.strptime(meta['start'], '%I:%M %p')
            date = datetime.strptime(meta['date'], '%a, %b %d, %Y')
            start = start.replace(
                year=date.year,
                month=date.month,
                day=date.day
            )

            if BOX_DAILY_MODE:
                # Only take todays records (if we're on daily mode).
                if date <= self.day:
                    continue
                # If today has stats.
                if not meta['url']:
                    continue

                print("Crawling url: " + meta['url'])

            url = self.base_url + meta['url']

            # Yield a single request for each game url.
            yield scrapy.Request(
                url=url,
                callback=self.parse,
                meta=meta
            )

    def parse(self, response):
        '''
        Parses four tables.

        '''

        # Grab all four tables.
        tables = response.xpath('//table[@class="sortable stats_table"]')

        # Parse date information from meta.
        datestring = response.meta['start'] + ', ' + response.meta['date']
        dt = datetime.strptime(datestring, '%I:%M %p, %a, %b %d, %Y')

        tables = {
            'basic_visitor': tables[0],
            'advanced_visitor': tables[1],
            'basic_home': tables[2],
            'advanced_home': tables[3]
        }

        # Iterate through basic tables.
        for key, table in tables.items():
            rows = table.xpath('tbody/tr')

            # Look at each row, collect statistics by player.
            for row in rows:

                # Get a new item.
                if 'basic' in key:
                    item = PlayerBasicItem()
                    headers = self.basic_headers
                elif 'advanced' in key:
                    item = PlayerAdvancedItem()
                    headers = self.advanced_headers
                else:
                    assert False

                # Fill meta data.
                item['timestamp'] = datetime.now()
                item['home'] = response.meta['home']
                item['visitor'] = response.meta['visitor']
                item['url'] = response.meta['url']
                item['datestring'] = datestring
                item['datetime'] = dt
                item['is_home'] = 'home' in key
                item['player'] = row.xpath('th/a/text()').extract_first()

                # Fill raw data.
                for stat in headers:
                    stat_path = f'td[@data-stat="{stat}"]/text()'
                    item[stat] = row.xpath(stat_path).extract_first()

                item['mp'] = self.convert_minutes(item['mp'])

                yield item

    def convert_minutes(self, mp):
        '''
        Converts minutes from '39:15' to 39.25 (float).

        '''
        try:
            splt = mp.split(':')
            return float(splt[0]) + float(splt[1]) / 60.
        except AttributeError:
            return None

    def advanced_box(self, table):
        '''
        Parses a single advanced_box table.

        '''
        pass
