# -*- coding: utf-8 -*-

# See models.py for more information on items.

import scrapy
from scrapy import Field


class PlayerBasicItem(scrapy.Item):
    timestamp = Field()
    url = Field()
    datestring = Field()
    datetime = Field()
    home = Field()
    visitor = Field()
    player = Field()
    is_home = Field()
    mp = Field()
    fg = Field()
    fg_pct = Field()
    fga = Field()
    fg3 = Field()
    fg3a = Field()
    ft_pct = Field()
    orb = Field()
    drb = Field()
    trb = Field()
    ast = Field()
    stl = Field()
    blk = Field()
    tov = Field()
    pf = Field()
    pts = Field()


class PlayerAdvancedItem(scrapy.Item):
    timestamp = Field()
    url = Field()
    datestring = Field()
    datetime = Field()
    home = Field()
    visitor = Field()
    player = Field()
    is_home = Field()
    mp = Field()
    ts_pct = Field()
    efg_pct = Field()
    fg3a_per_fga_pct = Field()
    fta_per_fga_pct = Field()
    orb_pct = Field()
    drb_pct = Field()
    trb_pct = Field()
    ast_pct = Field()
    stl_pct = Field()
    blk_pct = Field()
    tov_pct = Field()
    usg_pct = Field()
    off_rtg = Field()
    def_rtg = Field()


class MatchupItem(scrapy.Item):
    url = Field()
    month = Field()
    datestring = Field()
    timestring = Field()
    datetime = Field()
    visitor = Field()
    visitor_pts = Field()
    home = Field()
    home_pts = Field()
    box_url = Field()
    ot = Field()
    ot_count = Field()
