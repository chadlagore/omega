from __future__ import absolute_import
import datetime

from sqlalchemy import create_engine, UniqueConstraint, ForeignKey
from sqlalchemy import Boolean, Column, Integer, String, DateTime, Float
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL
from sqlalchemy.orm import relationship

import omega.settings as settings
from strings import *


DeclarativeBase = declarative_base()


def db_engine():
    '''
    Uses settings credentials to connect to the database.
    See enviroonment variable DATABASE_URL.

    '''
    return create_engine(URL(**settings.DATABASE))


def create_tables(engine):
    '''
    Creates tables in the database.

    '''
    DeclarativeBase.metadata.create_all(engine)


class PlayerBasic(DeclarativeBase):
    '''
    Schema representing basic box data.

    '''
    __tablename__ = PLAYER_BASIC

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, default=datetime.datetime.utcnow)
    url = Column(String, nullable=False)
    datestring = Column(String, nullable=False)
    datetime = Column(DateTime, nullable=False)
    home = Column(String, nullable=False)
    visitor = Column(String, nullable=False)
    player = Column(String, nullable=False)
    is_home = Column(Boolean, nullable=False)
    mp = Column(Float, nullable=True)
    fg = Column(Integer, nullable=True)
    fg_pct = Column(Float, nullable=True)
    fga = Column(Integer, nullable=True)
    fg3 = Column(Integer, nullable=True)
    fg3a = Column(Integer, nullable=True)
    ft_pct = Column(Float, nullable=True)
    orb = Column(Integer, nullable=True)
    drb = Column(Integer, nullable=True)
    trb = Column(Integer, nullable=True)
    ast = Column(Integer, nullable=True)
    stl = Column(Integer, nullable=True)
    blk = Column(Integer, nullable=True)
    tov = Column(Integer, nullable=True)
    pf = Column(Integer, nullable=True)
    pts = Column(Integer, nullable=True)

    # Foreign keys and relationships.
    matchup = relationship('Matchup', backref=PLAYER_BASIC)
    matchup_id = Column(
        Integer,
        ForeignKey('matchup.id', ondelete='CASCADE'),
        nullable=False,
    )

    # Constraints for table.
    __tableargs__ = (
        UniqueConstraint(
            'datestring',
            'visitor',
            'player',
            'home',
            name='unique_player_game'
        ),
        ForeignKeyConstraint(
            ['id'],
            ['matchup.id'],
            name='fk_box_matchup'
        )
    )


class PlayerAdvanced(DeclarativeBase):
    '''
    Advanced box score table.

    '''

    __tablename__ = PLAYER_ADVANCED

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, nullable=False)
    url = Column(String, nullable=False)
    datestring = Column(String, nullable=False)
    datetime = Column(DateTime, nullable=False)
    home = Column(String, nullable=False)
    visitor = Column(String, nullable=False)
    player = Column(String, nullable=False)
    is_home = Column(Boolean, nullable=False)
    mp = Column(Float, nullable=True)
    ts_pct = Column(Float, nullable=True)
    efg_pct = Column(Float, nullable=True)
    fg3a_per_fga_pct = Column(Float, nullable=True)
    fta_per_fga_pct = Column(Float, nullable=True)
    orb_pct = Column(Float, nullable=True)
    drb_pct = Column(Float, nullable=True)
    trb_pct = Column(Float, nullable=True)
    ast_pct = Column(Float, nullable=True)
    stl_pct = Column(Float, nullable=True)
    blk_pct = Column(Float, nullable=True)
    tov_pct = Column(Float, nullable=True)
    usg_pct = Column(Float, nullable=True)
    off_rtg = Column(Float, nullable=True)
    def_rtg = Column(Float, nullable=True)

    # Foreign keys and relationships.
    matchup = relationship('Matchup', backref=PLAYER_ADVANCED)
    matchup_id = Column(
        Integer,
        ForeignKey('matchup.id', ondelete='CASCADE'),
        nullable=True,
    )

    # Constraints for table.
    __tableargs__ = (
        UniqueConstraint(
            'datestring',
            'visitor',
            'player',
            'home',
            name='unique_player_game'
        ),
        ForeignKeyConstraint(
            ['id'],
            ['matchup.id'],
            name='fk_box_matchup'
        )
    )


class Matchup(DeclarativeBase):
    '''
    Schema representing a matchup and result.

    '''

    __tablename__ = MATCHUP

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, default=datetime.datetime.utcnow)
    url = Column(String, nullable=False)
    month = Column(String, nullable=False)
    datestring = Column(String, nullable=False)
    timestring = Column(String, nullable=False)
    datetime = Column(DateTime, nullable=False)
    visitor = Column(String, nullable=False)
    visitor_pts = Column(Integer, nullable=False)
    home = Column(String, nullable=False)
    home_pts = Column(Integer, nullable=False)
    box_url = Column(String, nullable=False)
    ot = Column(String, nullable=True)
    ot_count = Column(Integer, nullable=False)

    __tableargs__ = UniqueConstraint(
        'datestring',
        'timestring',
        'visitor',
        'home',
        name='unique_game'
    )

class MatchupAggregate(DeclarativeBase):
    '''
    Schema for aggregating matchup statistics by team.

    '''

    __tablename__ = MATCHUP_AGGREGATE

    team = Column(String, primary_key=True)
    opponent = Column(String, nullable=False)
    home_wins = Column(Integer, nullable=False)
    visiting_wins = Column(Integer, nullable=False)

    __tableargs__ = UniqueConstraint(
        'team',
        'opponent'
    )
