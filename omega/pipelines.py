from sqlalchemy.orm import sessionmaker

from .models import PlayerBasic, PlayerAdvanced, Matchup
from .models import db_engine, create_tables
from .items import PlayerBasicItem, PlayerAdvancedItem, MatchupItem

import omega.settings as settings
from strings import *

# A mapping of items to models.
models = {
    PlayerBasicItem: PlayerBasic,
    PlayerAdvancedItem: PlayerAdvanced,
    MatchupItem: Matchup
}


class PostgresPipeline(object):
    '''
    Pipeline for postgres transactions.

    '''

    def __init__(self):
        '''
        Creates connection and tables.

        '''
        print('# ======= CREATING TABLES =======  #')
        engine = db_engine()
        create_tables(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        '''
        Called for every item in the pipeline.
        Creates a record in the appropriate for the spider.

        '''

        # Lookup the model type, submit a record.
        record = models[item.__class__](**item)
        return self.submit_record(record)

    def submit_record(self, record):
        '''
        Tries to create a record in the table, on failure raises sql error.
        '''

        print('# ======= UPDATING TABLES =======  #')
        session = self.Session()
        try:
            session.add(record)
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            session.close()
