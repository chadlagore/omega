"""dupe_constraint_matchup

Revision ID: a3990f31a1fa
Revises: 50e2f4fb2f92
Create Date: 2017-04-12 10:06:35.036872

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a3990f31a1fa'
down_revision = '50e2f4fb2f92'
branch_labels = None
depends_on = None


def upgrade():
    op.create_unique_constraint(
        constraint_name='unique_game',
        table_name='matchup',
        columns=[
            'datestring',
            'timestring',
            'visitor',
            'home'
        ]
    )


def downgrade():
    op.drop_constraint('unique_game')
