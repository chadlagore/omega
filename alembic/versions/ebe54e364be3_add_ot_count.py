"""add_ot_count

Revision ID: ebe54e364be3
Revises:
Create Date: 2017-04-12 09:05:05.188351

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ebe54e364be3'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('matchup', sa.Column('ot_count', sa.Integer))

def downgrade():
    op.drop_column('matchup', 'ot_count')
