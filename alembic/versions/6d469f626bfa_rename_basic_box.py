"""rename_basic_box


Revision ID: 6d469f626bfa
Revises: a3990f31a1fa
Create Date: 2017-04-17 17:09:38.477889

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6d469f626bfa'
down_revision = 'a3990f31a1fa'
branch_labels = None
depends_on = None


def upgrade():
    op.rename_table('basic_box_raw', 'player_basic')


def downgrade():
    op.rename_table('player_basic', 'basic_box_raw')
