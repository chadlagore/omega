"""nullable_ot

Revision ID: 50e2f4fb2f92
Revises: 97aa59f929c6
Create Date: 2017-04-12 09:38:07.850199

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '50e2f4fb2f92'
down_revision = '97aa59f929c6'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        table_name='matchup',
        column_name='ot',
        nullable=True
    )


def downgrade():
    op.alter_column(
        table_name='matchup',
        column_name='ot',
        nullable=False
    )
