"""add_matchup_id_to_box

Revision ID: 97aa59f929c6
Revises: ebe54e364be3
Create Date: 2017-04-12 09:10:45.655402

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import INTEGER, Column, ForeignKey


# revision identifiers, used by Alembic.
revision = '97aa59f929c6'
down_revision = 'ebe54e364be3'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('basic_box_raw',
        Column('matchup_id', INTEGER, ForeignKey('matchup.id'))
    )
    op.create_foreign_key(
        'fk_box_matchup',
        'basic_box_raw',
        'matchup',
        local_cols=['matchup_id'],
        remote_cols=['id']
    )

def downgrade():
    op.delete_column('basic_box_raw', 'matchup_id')
    op.drop_constraint(
        'fk_box_matchup',
        'basic_box_raw'
    )
